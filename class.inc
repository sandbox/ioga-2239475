<?php

//===========================================================================
 /**
 * View the form class submissions
 */
 function form_class_submissions(){

    $query = db_select('sia_class', 'c')
      ->fields('c')
      ->extend('PagerDefault')
      ->limit(10)
      ->orderBy('class_name', 'asc')
      ->execute();

    $header = array(
      array('data' => t('No'), 'style' => 'text-align:center'),
      array('data' => t('Nama Kelas'), 'style' => 'text-align:center'),
      array('data' => t('Keterangan'), 'style' => 'text-align:center'),
      array('data' => t('Hapus'), 'style' => 'text-align:center'),
      );
    $rows = array();

    $limit=10;
    if (isset($_GET['page'])){
      $i=$limit*$_GET['page']+1;
    }
    else{
      $i=1;
    }

    foreach ($query AS $result){
        $rows[] = array(
        array('data' => $i++, 'style' => 'text-align:center'),
        array('data' => l(check_plain($result->class_name),"edit-class/{$result->class_name}")),
        array('data' => $result->description),
        array('data' => l('Delete',"delete-class/{$result->class_name}"), 'style' => 'text-align:center'),
      );
    }

    //$output = theme("table", array('header' => $header, 'rows' => $rows));
    $output = theme_table(
            array(
              "header" => $header,
              "rows" => $rows,
              "attributes" => array(),
              "sticky" => true, // Table header will be sticky
              "caption" => "",
              "colgroups" => array(),
              "empty" => t("Table has no data!") // The message to be displayed if table is empty
            )
      );
    $output .= theme('pager', array('tags' => array()));
    $output .= l('<button>' . t('Tambah') . '</button>', 'class/add-class', array('html' => TRUE));

    return $output;
   }


/**
 * create form for creating new class
 */

 function form_class_form($form, &$form_state){

  $form['class_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama Kelas'),
    '#size' => 10,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#description' => t('Masukkan nama kelas.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Keterangan'),
    '#description' => t('Masukkan keterangan kelas.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );

  $_GET['destination'] = 'class/view-class';

  return $form;

 }


  /**
  * validation for form class form
  */
  function form_class_form_validate($form, &$form_state) {
    $kelas = $form_state['values']['class_name'];
    $sql = db_query("select * from sia_class where class_name = '{$kelas}'");

    if ($sql->rowCount() > 0) {
        form_set_error('class_name', t("Kelas dengan nama {$kelas} sudah ada ."));
    }
  }


 /**
  *  submit handler for form_subject_form, insert data into database
  */
 function form_class_form_submit($form, &$form_state){
    $subject_id = db_insert('sia_class')
      ->fields(array(
        'class_name' => $form_state['values']['class_name'],
        'description' => $form_state['values']['description'],
      ))
      ->execute();

      drupal_set_message(t('Kelas telah ditambahkan.'));
   }

 /**
  *  delete data on database
  */
 function delete_class($arg1){
  try {
  $num_deleted = db_delete('sia_class')
    ->condition('class_name', $arg1)
    ->execute();
  drupal_set_message(t("Kelas {$arg1} berhasil dihapus."));
  }
  catch (Exception $e) {
    drupal_set_message(t("Tidak dapat dihapus karena berelasi dengan data lain."), 'error');
  }
  drupal_goto("class/view-class");
 }

/**
 * create form for editing class
 */

 function edit_class($form, &$form_state,$arg1){
  if(isset($arg1) && !empty($arg1)){
  $result = db_query("select * from sia_class where class_name= '{$arg1}' ");
    if ($result->rowCount() < 1){
      drupal_goto("class/view-class");
    }
    else{
      $row = array();
      foreach($result as $record){
        $row = (array)$record;
      }
    }
  }
  else{
     drupal_goto("class/view-class");
  }
  $form['class_name_old'] = array(
    '#type' => 'hidden',
    '#default_value' => $row['class_name'],
  );
  $form['class_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama kelas'),
    '#size' => 30,
    '#maxlength' => 30,
    '#default_value' => $row['class_name'],
    '#required' => TRUE,
    '#description' => t('Masukkan nama kelas.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Keterangan'),
    '#default_value' => $row['description'],
    '#description' => t('Keterangan kelas.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );

  $_GET['destination'] = 'class/view-class';

  return $form;

 }

 /**
  *  submit handler for edit_class
  */
 function edit_class_submit($form, &$form_state){
    $subject_id = db_update('sia_class')
      ->fields(array(
        'class_name' => $form_state['values']['class_name'],
        'description' => $form_state['values']['description'],
      ))
      ->condition('class_name', $form_state['values']['class_name_old'])
      ->execute();

      drupal_set_message(t('Perubahan telah disimpan.'));
   }
