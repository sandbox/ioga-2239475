<?php
/**
 * function to show student test result
 */
 function show_test_result($suid=null){

    if(!empty($_GET['th_ajaran'])){
        $year = $_GET['th_ajaran'];
    }
    else {
    $year_act = db_select('sia_year','y')
        ->fields('y', array('year_active'))
        ->execute()
        ->fetchAssoc();
    $year = $year_act['year_active'];
    }
    $sql = db_select('sia_student_data','st');
    $sql->leftJoin('sia_kkm','kkm','kkm.kkm_id = st.kkm_id');
    $sql->leftJoin('sia_test_result','tr','tr.test_result_id = st.test_result_id');
    $sql->condition('st.year_active',$year);
    $sql->condition('st.suid',$suid);
    $run_sql = $sql
        ->fields('kkm')
        ->fields('st')
        ->fields('tr', array(
            'test_result_id',
            'test1',
            'test2',
            'test3',
            'test4',
            'mid',
            'uas',
            ))
        ->execute();
   $i=1;
    $header = array(
      array('data' => t('No'), 'style' => 'text-align:center'),
      array('data' => t('Mata Pelajaran'), 'style' => 'text-align:center'),
      array('data' => t('T1'), 'style' => 'text-align:center'),
      array('data' => t('T2'), 'style' => 'text-align:center'),
      array('data' => t('T3'), 'style' => 'text-align:center'),
      array('data' => t('T4'), 'style' => 'text-align:center'),
      array('data' => t('Mid'), 'style' => 'text-align:center'),
      array('data' => t('UAS'), 'style' => 'text-align:center'),
      );
    $rows = array();

    foreach ($run_sql AS $result){
      $rows[] = array(
        array('data' => $i++, 'style' => 'text-align:center'),
        array('data' => $result->subject_name),
        array('data' => $result->test1, 'style' => 'text-align:center'),
        array('data' => $result->test2, 'style' => 'text-align:center'),
        array('data' => $result->test3, 'style' => 'text-align:center'),
        array('data' => $result->test4, 'style' => 'text-align:center'),
        array('data' => $result->mid, 'style' => 'text-align:center'),
        array('data' => $result->uas, 'style' => 'text-align:center'),
      );
    }

    $tbl = theme("table", array('header' => $header, 'rows' => $rows, 'empty' => t('Tidak ada data ditemukan.')));
    $tbl .= theme('pager', array('tags' => array()));

    $build['output'] = array(
        'form_search' => drupal_get_form('test_search_form', $suid),
        'show_title' => array('#markup' => t("<b><center>Data nilai tahun ajaran {$year}</center></b>") ),
        'show_table' => array('#markup' => $tbl, ),
    );
    return $build;
 }


/**
 * function to edit student test result
 */
 function student_test_result($suid=null, $subject_id=null, $action=null){
    $build['check'] = array(
        'form_check' => drupal_get_form('check_subject_form',$suid),
        'form_table' => drupal_get_form('ftable', array($suid, $subject_id)),
        //'show_table' => array('#markup' => $tbl, ),
    );

    return $build;
 }

/**
 * form check available subject and auto create sheet for each subject
 */

 function check_subject_form($form, &$form_state, $suid){
    $form = array();
    $form['suid'] = array(
    '#type' => 'hidden',
    '#value' => $suid,
    );
    $form['check'] = array(
    '#type' => 'submit',
    '#suffix' => t('<b> Klik tombol check untuk memeriksa mata pelajaran yang ditawarkan.</b>'),
    '#value' => t('check'),
    );
    $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
    );
    $_GET['destination'] ="students/{$suid}/detail-student/test-result";
    return $form;
 }

/**
 * submit handler form check_subject_form
 */

 function check_subject_form_submit($form, &$form_state){
    // deklarasi untuk mengabil data user
    global $user;

    $student = db_select('sia_students','s')
        ->fields('s')
        ->condition('suid',$form_state['values']['suid'])
        ->execute()
        ->fetchAssoc();
    //dpm($student['nis']);

    $year = db_select('sia_year','y')
        ->fields('y')
        ->execute()
        ->fetchAssoc();
    //dpm($year);
    $cek_mapel = db_select('sia_kkm','k')
        ->fields('k')
        ->condition('class_name',$student['class_name'])
        ->execute();
    //dpm($cek_mapel);
    $get_mapel='';
    foreach ($cek_mapel as $mapel){
        $cek_data = db_select('sia_student_data','d')
            ->fields('d')
            ->condition('suid',$student['suid'])
            ->condition('kkm_id',$mapel->kkm_id)
            ->condition('year_active',$year['year_active'])
            ->execute()
            ->rowCount();

        if($cek_data < 1){
            $test_id = db_insert('sia_test_result')
                ->fields(array(
                    'test1' => 0,
                    'test2' => 0,
                    'test3' => 0,
                    'test4' => 0,
                    'mid' => 0,
                    'uas' => 0,
                    'last_result' => 0,
                    'created' => date('Y-m-h H:m:s'),
                    'created_by' => $user->uid,
                    )
                  )
                ->execute();

            $sdt_id = db_insert('sia_student_data')
                ->fields(array(
                    'suid' => $form_state['values']['suid'],
                    'kkm_id' => $mapel->kkm_id,
                    'test_result_id' => $test_id,
                    'year_active' => $year['year_active'],
                ))
                ->execute();
            $get_mapel=$mapel->subject_name .", ".$get_mapel;
            //dpm($sdt_id);

        }
    }
    if($get_mapel=='') drupal_set_message('Tidak ada data baru');
    else drupal_set_message("Mata pelajaran {$get_mapel} telah ditambahkan");
    $form_state['rebuild'] = TRUE;

 }

/**
 * delete test result
 */
 function delete_test_result($suid, $tid){
    try{
    $del_id = db_delete('sia_student_data')
        ->condition('test_result_id',$tid)
        ->execute();
    $del_test = db_delete('sia_test_result')
        ->condition('test_result_id',$tid)
        ->execute();
    drupal_set_message(t("Data test telah dihapus."));
    }
    catch (Exception $e){
        drupal_set_message(t("Gagal menghapus <br> {$e}."), 'error');
    }
    drupal_goto("students/{$suid}/detail-student/test-result");
 }

/**
 * function ftable
 */
 function ftable($form, &$form_state, $suid=null, $subject_id=null){

$form['test'] = array(
    '#prefix' => '<div id="mymodule_button">',
    '#suffix' => '</div>',
);
$form['table_test'] = array(
  '#tree' => TRUE,
  '#theme' => 'table',
  '#header' => array(
        array('data' => t('Mata Pelajaran'), 'style' => 'text-align:center'),
        array('data' => t('T1'), 'style' => 'text-align:center'),
        array('data' => t('T2'), 'style' => 'text-align:center'),
        array('data' => t('T3'), 'style' => 'text-align:center'),
        array('data' => t('T4'), 'style' => 'text-align:center'),
        array('data' => t('Mid'), 'style' => 'text-align:center'),
        array('data' => t('UAS'), 'style' => 'text-align:center'),
        array('data' => t('save'), 'style' => 'text-align:center'),
        array('data' => t('hapus'), 'style' => 'text-align:center'),
    ),
  '#rows' => array(),
);
    $year = db_select('sia_year','y')
        ->fields('y')
        ->execute()
        ->fetchAssoc();

    $sql = db_select('sia_student_data','st');
    $sql->leftJoin('sia_kkm','kkm','kkm.kkm_id = st.kkm_id');
    //$sql->leftJoin('sia_students','s','s.class_name = kkm.class_name');
    $sql->leftJoin('sia_test_result','tr','tr.test_result_id = st.test_result_id');
    $run_sql = $sql
        ->fields('kkm')
        ->fields('st')
        ->fields('tr', array(
            'test_result_id',
            'test1',
            'test2',
            'test3',
            'test4',
            'mid',
            'uas',
            ))
        ->condition('st.suid',$suid)
        ->condition('st.year_active',$year['year_active'])
        ->execute();
    foreach ($run_sql AS $result){

    $mapel = array(
    '#type' => 'item',
    '#size' => 20,
    '#disabled' => TRUE,
    '#title' => $result->subject_name,
    );
    $test1 = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->test1,
    //'#element_validate' => array('element_validate_number'),
    );
    $test2 = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->test2,
    //'#element_validate' => array('element_validate_number'),
    );
    $test3 = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->test3,
    //'#element_validate' => array('element_validate_number'),
    );
    $test4 = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->test4,
    //'#element_validate' => array('element_validate_number'),
    );
    $mid = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->mid,
    //'#element_validate' => array('element_validate_number'),
    );
    $uas = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $result->uas,
    //'#element_validate' => array('element_validate_number'),
    );

    $edit = array(
    '#type' => 'button',
    '#name' => $result->test_result_id,
    '#value' => t('save'),
    '#ajax' => array(
      'callback' => 'edit_test_callback',
      'wrapper' => 'mymodule_button',
      'method' => 'replace',
      'effect' => 'fade',
        ),
    );

    $del = array(
    '#name' => $result->test_result_id,
    '#type' => 'submit',
    '#value' => t('X'),
    '#submit' => array('del_test_result'),
  );

    $form['table_test'][$result->test_result_id] = array(
    'mapel.' => &$mapel,
    'test1' => &$test1,
    'test2' => &$test2,
    'test3' => &$test3,
    'test4' => &$test4,
    'mid' => &$mid,
    'uas' => &$uas,
    'edit' => &$edit,
    'del' => &$del,
    );

    $form['table_test']['#rows'][] = array(
    array('data' => &$mapel),
    array('data' => &$test1),
    array('data' => &$test2),
    array('data' => &$test3),
    array('data' => &$test4),
    array('data' => &$mid),
    array('data' => &$uas),
    array('data' => &$edit),
    array('data' => &$del),
    );

      unset($mapel);
      unset($test1);
      unset($test2);
      unset($test3);
      unset($test4);
      unset($mid);
      unset($uas);
      unset($edit);
      unset($del);
    }
    //dpm($form['table_test']);
 return $form;
 }

function edit_test_callback($form, $form_state) {
  // get value from the triggering element
  global $user;
  $test_id = $form_state['triggering_element']['#name'];
  $formula =((   $form_state['values']['table_test'][$test_id]['test1'] +
                    $form_state['values']['table_test'][$test_id]['test2'] +
                    $form_state['values']['table_test'][$test_id]['test3'] +
                    $form_state['values']['table_test'][$test_id]['test4']
                )/4 +
                    $form_state['values']['table_test'][$test_id]['mid']+
                    $form_state['values']['table_test'][$test_id]['uas']
                )/3;
    $last_result = ceil($formula);
  $sql = db_update('sia_test_result')
    -> fields(array(
        'test1' => $form_state['values']['table_test'][$test_id]['test1'],
        'test2' => $form_state['values']['table_test'][$test_id]['test2'],
        'test3' => $form_state['values']['table_test'][$test_id]['test3'],
        'test4' => $form_state['values']['table_test'][$test_id]['test4'],
        'mid' => $form_state['values']['table_test'][$test_id]['mid'],
        'uas' => $form_state['values']['table_test'][$test_id]['uas'],
        'last_result' => $last_result,
        'edited' => date('Y-m-d'),
        'edited_by' => $user->uid,
        ))
    ->condition('test_result_id',$test_id);
    try{
        $sql->execute();
        drupal_set_message(t("Nilai berhasil diubah"));
    }catch(PDOException $e){
        drupal_set_message(t('Error : Nilai gagal diubah, nilai harus angka'),'error');
    }
  // run the script
  //some_script($value);
  return "<div id='mymodule_button'></div>";
}

function del_test_result($form, $form_state) {
  // get value from the triggering element
  global $user;
  $test_id = $form_state['triggering_element']['#name'];
  try{
  $sql_std = db_delete('sia_student_data')
    ->condition('test_result_id',$test_id)
    ->execute();
  $sql_ts = db_delete('sia_test_result')
    ->condition('test_result_id',$test_id)
    ->execute();
  }catch(PDOException $e) {
    drupal_set_message(t('Error : %message',array('%message' => $e)),'error');
  }
  // run the script
  //some_script($value);
}

/**
 * custom search form
 */
 function teacher_search_form($form, &$form_state) {
  $form = array();
  $status = array(
    'aktif' => 'Aktif',
    'mutasi' => 'Mutasi',
    'pensiun' => 'Pensiun',
  );

  $form['advance_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advance search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance_search']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama pengajar'),
    '#default_value' => "",
    '#description' => "Masukkan nama pengajar yang dicari.",
    '#size' => 20,
    '#maxlength' => 20,
  );
  $form['advance_search']['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
    ),
  );
  $form['advance_search']['container']['status'] = array(
    '#type' => 'select',
    '#title' => t('Cari berdasarkan '),
    '#empty_option' => '- ' . t('status') . ' -',
    '#options' => $status,
  );
  $form['advance_search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
  );

  return $form;
 }

 /**
 * custom search form submit handler
 */
 function teacher_search_form_submit($form, &$form_state) {
  $path = t('teachers/view-teacher/');
  $options = array(
    'query' => array(
      'name' => $form_state['values']['name'],
      'status' => $form_state['values']['status'],
      )
    );
  return drupal_goto($path, $options);
 }
/**
 * custom search form
 */
 function test_search_form($form, &$form_state, $suid) {
  $form = array();
  $sql = db_query("select year_active from sia_student_data where suid= {$suid}");
    foreach ($sql as $row){
        $th[$row->year_active] = $row->year_active;
      }
  $form['suid'] = array(
    '#type' => 'hidden',
    '#default_value' => $suid,
  );
  $form['advance_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance_search']['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
    ),
  );
  $form['advance_search']['container']['th_ajaran'] = array(
    '#type' => 'select',
    '#title' => t('Cari berdasarkan '),
    '#empty_option' => '- ' . t('tahun ajaran - smt') . ' -',
    '#options' => $th,
    '#ajax' => array(
      'callback' => 'class_field_callback',
      'wrapper' => 'textfields',
      'effect' => 'fade',
    ),
  );
  $form['advance_search']['container']['class'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="textfields">',
    '#suffix' => '</div>',
  );
  if (!empty($form_state['values']['th_ajaran'])) {
    $yr = $form_state['values']['th_ajaran'];
    $sql_class = db_query("select distinct(class_name) from sia_kkm left join sia_student_data using(kkm_id) where (sia_student_data.year_active='{$yr}')");
    foreach ($sql_class as $class);
    $form['advance_search']['container']['class']['#title'] = t("Kelas {$class->class_name}");
  }
  $form['advance_search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
  );

  return $form;
 }

 /**
 * custom search form submit handler
 */
 function test_search_form_submit($form, &$form_state) {
  $suid = $form_state['values']['suid'];
  $path = t("students/{$suid}/detail-student/show-test-result/");
  $options = array(
    'query' => array(
      'th_ajaran' => $form_state['values']['th_ajaran'],
      )
    );
  return drupal_goto($path, $options);
 }

// callback ajax class
function class_field_callback($form, $form_state) {
  return $form['advance_search']['container']['class'];
}
