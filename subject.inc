<?php
/**
 * View the form subject submissions
 */
 function form_subject_submissions(){

    $query = db_select('sia_subjects', 's')
      ->fields('s')
      ->extend('PagerDefault')
      ->limit(10)
      ->orderBy('subject_name', 'asc')
      ->execute();

    $limit=10;
    if (isset($_GET['page'])){
      $i=$limit*$_GET['page']+1;
    }
    else{
      $i=1;
    }

    $header = array(
      array('data' => t('No'), 'style' => 'text-align:center'),
      array('data' => t('Mata Pelajaran'), 'style' => 'text-align:center'),
      array('data' => t('Keterangan'), 'style' => 'text-align:center'),
      array('data' => t('Hapus'), 'style' => 'text-align:center'),
      );
    $rows = array();

    foreach ($query AS $result){
      $rows[] = array(
        array('data' => $i++, 'style' => 'text-align:center'),
        array('data' => l(check_plain($result->subject_name),"edit-subject/{$result->subject_name}")),
        array('data' => $result->description),
        array('data' => l('Delete',"delete-subject/{$result->subject_name}"), 'style' => 'text-align:center'),
      );
    }

    //$output = theme("table", array('header' => $header, 'rows' => $rows));
     $output = theme_table(
            array(
              "header" => $header,
              "rows" => $rows,
              "attributes" => array(),
              "sticky" => true, // Table header will be sticky
              "caption" => "",
              "colgroups" => array(),
              "empty" => t("Table has no data!") // The message to be displayed if table is empty
            )
      );
    $output .= theme('pager', array('tags' => array()));
    //$output .= l('Tambah', 'subjects/add-subject');
    $output .= l('<button>' . t('Tambah') . '</button>', 'subjects/add-subject', array('html' => TRUE));

    return $output;
   }

 /**
 * create form for creating new subject
 */

 function sia_sekolah_subject_form($form, &$form_state){
  $form['subject_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama Pelajaran'),
    '#size' => 30,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('Masukkan nama pelajaran'),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Keterangan'),
    '#default' => '',
    '#description' => t('Keterangan pelajaran'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );
  $_GET['destination'] = 'subjects/view-subject';
  return $form;
 }

 /**
  *  submit handler for sia_sekolah_subject_form, insert data into database
  */
 function sia_sekolah_subject_form_submit($form, &$form_state){
  try {
    $subject_id = db_insert('sia_subjects')
      ->fields(array(
        'subject_name' => $form_state['values']['subject_name'],
        'description' => $form_state['values']['description'],
      ))
      ->execute();
      drupal_set_message(t('Mata pelajaran telah disimpan.'));
   }
  catch (Exception $e) {
    drupal_set_message(t("Mata pelajaran sudah ada."), 'error');
  }
  drupal_goto("subjects/view-subject");
 }

 /**
  *  delete data on database
  */
  function delete_subject($arg1){
  try {
  $num_deleted = db_delete('sia_subjects')
    ->condition('subject_name', $arg1)
    ->execute();
  drupal_set_message(t("Mata pelajaran {$arg1} telah dihapus."));
  }
  catch (Exception $e) {
    drupal_set_message(t("Tidak dapat dihapus karena berelasi dengan data lain."), 'error');
  }
  drupal_goto("subjects/view-subject");

 }

/**
 * create form for editing subject
 */

 function edit_subject($form, &$form_state,$arg1){
  if(isset($arg1) && !empty($arg1)){
  $result = db_query("select * from sia_subjects where subject_name = '{$arg1}' ");
    if ($result->rowCount() < 1){
      header("Location: subjects/view-subject");
    }
    else{
      $row = array();
      foreach($result as $record){
        $row = (array)$record;
      }
    }
  }
  else{
    header("Location: subjects/view-subject");
  }
  $form['subject_name_old'] = array(
    '#type' => 'hidden',
    '#default_value' => $row['subject_name'],
  );

  $form['subject_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama Pelajaran'),
    '#size' => 30,
    '#maxlength' => 30,
    '#default_value' => $row['subject_name'],
    '#required' => TRUE,
    '#description' => t('Edit nama pelajaran'),
  );


  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Keterangan'),
    '#default_value' => $row['description'],
    '#description' => t('Keterangan pelajaran'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );

  $_GET['destination'] = 'subjects/view-subject';

  return $form;
 }

 /**
  *  submit handler for edit_subject
  */
 function edit_subject_submit($form, &$form_state){
    $subject_id = db_update('sia_subjects')
      ->fields(array(
        'subject_name' => $form_state['values']['subject_name'],
        'description' => $form_state['values']['description'],
      ))
      ->condition('subject_name', $form_state['values']['subject_name_old'])
      ->execute();

      drupal_set_message(t('Perubahan telah disimpan.'));
   }
