<?php

//===========================================================================
 /**
 * View kkm
 */

 function form_add_kkm($form, &$form_state) {

  $subject = db_query('select subject_name from sia_subjects');
  $class = db_query('select class_name from sia_class');

  if ( ($subject->rowCount() < 1) || ($class->rowCount() < 1) ){
    t('Silahkan isi dulu data mata pelajaran dan kelas');
  }
  else{
      $row_subject=array();
      $row_class=array();
      foreach ($subject as $row){
        $row_subject[$row->subject_name] = $row->subject_name;
      }
      foreach ($class as $row){
        $row_class[$row->class_name] = $row->class_name;
      }
  }
  $form = array();
  $form['subject_name'] = array(
  '#title' => t('Pilih mata pelajaran'),
  '#type' => 'select',
  '#multiple_toggle' => '1',
  '#options' => $row_subject,
  );
  $form['class_name'] = array(
  '#title' => t('Pilih kelas'),
  '#type' => 'select',
  '#multiple_toggle' => '1',
  '#options' => $row_class,
  );
  $form['kkm'] = array(
    '#type' => 'textfield',
    '#title' => t('kkm'),
    '#size' => 5,
    '#maxlength' => 3,
    '#default_value' => 1,
    '#required' => TRUE,
    '#description' => t('Masukkan kkm (1 - 100)'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );
  $_GET['destination'] = 'kkm/view-kkm';
  return $form;
  }


  /*
    * extra validation for kkm form
  */
  function form_add_kkm_validate($form, &$form_state) {
    $subject_name = $form_state['values']['subject_name'];
    $class_name = $form_state['values']['class_name'];
    $kkm = $form_state['values']['kkm'];
    $sql = db_query("select * from sia_kkm where subject_name = '{$subject_name}' and class_name = '{$class_name}' ");
    if ($sql->rowCount() > 0) {
        form_set_error('', t("KKM untuk pelajaran {$subject_name} kelas {$class_name} sudah ada ."));
    }
    if ($kkm > 100) {
         form_set_error('kkm', t('KKM tidak boleh lebih dari 100.'));
    }
}

/*
  * submit handler form kkm
*/
function form_add_kkm_submit($form, &$form_state) {
  $subject_id = db_insert('sia_kkm')
      ->fields(array(
        'subject_name' => $form_state['values']['subject_name'],
        'class_name' => $form_state['values']['class_name'],
        'kkm' => $form_state['values']['kkm'],
      ))
      ->execute();

  drupal_set_message(t("KKM berhasil disimpan."));
}

//===========================================================================
 /**
 * View the form class submissions
 */
  function view_kkm(){

    $query = db_select('sia_kkm', 'k')
      ->fields('k')
      ->extend('PagerDefault')
      ->limit(10)
      ->orderBy('class_name')
      ->execute();

    $header = array(
      array('data' => t('No'), 'style' => 'text-align:center'),
      array('data' => t('Mata Pelajaran'), 'style' => 'text-align:center'),
      array('data' => t('Kelas'), 'style' => 'text-align:center'),
      array('data' => t('KKM'), 'style' => 'text-align:center'),
      array('data' => t('Hapus'), 'style' => 'text-align:center'),
      );
    $rows = array();

    $limit=10;
    if (isset($_GET['page'])){
      $i=$limit*$_GET['page']+1;
    }
    else{
      $i=1;
    }

    foreach ($query AS $result){
      $rows[] = array(
        array('data' => $i++, 'style' => 'text-align:center'),
        array('data' => $result->subject_name),
        array('data' => $result->class_name, 'style' => 'text-align:center'),
        array('data' =>  l(check_plain($result->kkm),"kkm/{$result->kkm_id}/edit-kkm"), 'style' => 'text-align:center'),
        array('data' => l('Hapus',"kkm/{$result->kkm_id}/delete-kkm"), 'style' => 'text-align:center'),
      );
    }

    $output = theme_table(
            array(
              "header" => $header,
              "rows" => $rows,
              "attributes" => array(),
              "sticky" => true, // Table header will be sticky
              "caption" => "",
              "colgroups" => array(),
              "empty" => t("Table has no data!") // The message to be displayed if table is empty
            )
      );
    $output .= theme('pager', array('tags' => array()));
    $output .= l('<button>' . t('Tambah') . '</button>', 'kkm/new-kkm', array('html' => TRUE));

    return $output;

  }

  /**
  *  delete data on database
  */
 function delete_kkm($arg1){
  try {
  $num_deleted = db_delete('sia_kkm')
    ->condition('kkm_id', $arg1)
    ->execute();
  drupal_set_message(t('Data berhasil dihapus.'));
  }
  catch (Exception $e) {
    drupal_set_message(t("Tidak dapat dihapus karena berelasi dengan data lain."), 'error');
  }
  drupal_goto("kkm/view-kkm");
 }


/**
 * create form for edit kkm
 */
function form_edit_kkm($form, &$form_state,$arg1) {

  if(isset($arg1) && !empty($arg1)){
  $result = db_query("select * from sia_kkm where kkm_id = '{$arg1}' ");
    if ($result->rowCount() < 1){
      drupal_goto("kkm/view-kkm");
    }
    else{
      $row = array();
      foreach($result as $record){
        $row = (array)$record;
      }
    }
  }
  else{
    drupal_goto("kkm/view-kkm");
  }
  $form = array();

  $form['kkm_id'] = array(
  '#type' => 'hidden',
  '#default_value' => $row['kkm_id'],
  );

  $form['subject_name'] = array(
  '#title' => t('Mata pelajaran'),
  '#type' => 'textfield',
  '#size' => 5,
  '#maxlength' => 5,
  '#disabled' => TRUE,
  '#default_value' => $row['subject_name'],
  );

  $form['class_name'] = array(
  '#title' => t('Kelas'),
  '#type' => 'textfield',
  '#size' => 5,
  '#maxlength' => 5,
  '#disabled' => TRUE,
  '#default_value' => $row['class_name'],
  );

  $form['kkm'] = array(
    '#type' => 'textfield',
    '#title' => t('kkm'),
    '#size' => 5,
    '#maxlength' => 3,
    '#default_value' => $row['kkm'],
    '#required' => TRUE,
    '#description' => t('Masukkan kkm (1 - 100)'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );

  $_GET['destination'] = 'kkm/view-kkm';

  return $form;
  }

/**
  *  submit handler for edit_class
  */
 function form_edit_kkm_submit($form, &$form_state){
    $subject_id = db_update('sia_kkm')
      ->fields(array(
        'kkm' => $form_state['values']['kkm'],
      ))
      ->condition('kkm_id', $form_state['values']['kkm_id'])
      ->execute();
      drupal_set_message(t('Perubahan telah disimpan.'));
   }

/**
 * Validate handler for form edit_kkm.
 */
function form_edit_kkm_validate($form, &$form_state) {
  $kkm = $form_state['values']['kkm'];
  if ($kkm > 100) {
         form_set_error('kkm', t('KKM tidak boleh lebih dari 100.'));
    }
}
