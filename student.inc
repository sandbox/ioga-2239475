<?php

/**
  * Create form for insert data student and register new user
**/

function add_student_form($form, &$form_state) {
  // Display page 2 if $form_state['page_num'] == 2
  if (!empty($form_state['page_num']) && $form_state['page_num'] == 2) {
    return add_student_two($form, $form_state);
  }

  $jk = array(
    'L' => 'Laki-laki',
    'P' => 'Perempuan',
  );

  $ag = array(
    'Islam' => 'Islam',
    'Katolik' => 'Katolik',
    'Kristen' => 'Kristen',
    'Budha' => 'Budha',
    'Hindu' => 'Hindu',
  );

$class = db_query('select class_name from sia_class');

  if ( $class->rowCount() < 1 ){
    t('Silahkan isi dulu data kelas');
  }
  else{
      $row_class=array();
      foreach ($class as $row){
        $row_class[$row->class_name] = $row->class_name;
      }
  }

  // Otherwise we build page 1.
  $form_state['page_num'] = 1;
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Input data siswa baru.'),
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama lengkap'),
    '#size' =>35,
    '#maxlength' => 35,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['nis'] = array(
    '#type' => 'textfield',
    '#title' => t('NIS'),
    '#description' => "Masukkan nomor induk siswa (akan digunakan sebagai user wali).",
    '#size' =>12,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['address'] = array(
    '#type' => 'textarea',
    '#title' => t('Alamat'),
    '#size' =>50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['birthdate'] = array(
    '#title' => t('Tanggal lahir'),
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#required' => TRUE,
  //'#attributes' => array('class' => array('datepicker'),'placeholder' => t('dd/mm/YYYY')),
  );
  $form['birthplace'] = array(
    '#type' => 'textfield',
    '#title' => t('Tempat lahir'),
    '#size' =>50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['jk'] = array(
    '#type' => 'radios',
    '#title' => t('Jenis Kelamin'),
    '#description' => t(''),
    '#default_value' =>  variable_get('jk', 'L'),
    '#options' => $jk,
  );
  $form['religion'] = array(
    '#title' => t('Agama'),
    '#type' => 'select',
    '#multiple_toggle' => '1',
    '#options' => $ag,
  );
  $form['class'] = array(
    '#title' => t('Kelas'),
    '#type' => 'select',
    '#multiple_toggle' => '1',
    '#options' => $row_class,
  );
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'Selanjutnya >>',
    '#submit' => array('add_student_next_submit'),
    '#validate' => array('add_student_next_validate'),
  );

  //penting biar date picker jalan
  $form['#after_build'] = array('sia_sekolah_form_uidatepicker');
  return $form;
}

/**
 * Returns the form for the second page.
 */
function add_student_two($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Data wali murid'),
  );
  $form['guardian_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama lengkap wali'),
    '#size' =>35,
    '#maxlength' => 35,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['guardian_address'] = array(
    '#type' => 'textarea',
    '#title' => t('Alamat'),
    '#size' =>50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['job'] = array(
    '#type' => 'textfield',
    '#title' => t('Pekerjaan'),
    '#size' =>50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' =>50,
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' =>'',
    '#attributes' =>array('placeholder' => t('example@email.com'))
  );
  $form['telp'] = array(
    '#type' => 'textfield',
    '#title' => t('Telp.'),
    '#size' =>20,
    '#maxlength' => 12,
    '#required' => TRUE,
    '#default_value' =>'',
    '#attributes' =>array('placeholder' => t('+620000000000'))
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
    '#validate' => array('add_student_two_validate'),
    '#submit' => array('add_student_two_submit'),
  );
  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('<< Kembali'),
    "#weight" => 15,
    "#executes_submit_callback" => FALSE,
    '#limit_validation_errors' => array(),
    '#ajax' => array(
    'callback' => 'add_student_two_back'
    )
  );

  $_GET['destination'] = 'students/view-student';

  return $form;
}
/**
 * Validate handler for the next button on first page.
 */
function add_student_next_validate($form, &$form_state) {

  // validate nis
  $nis = db_select('sia_students','s')
    ->fields('s')
    ->condition('nis',$form_state['values']['nis'])
    ->execute();
  if ($nis->rowCount() > 0){
    form_set_error('nis',t('Nomor Induk Siswa telah digunakan.'));
  }
}
/**
 * Validate handler for the submit button on second page.
 */
function add_student_two_validate($form, &$form_state) {

  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail',t('Pastikan format email benar.'));
  }
}
/**
 * Submit handler for kaisiq_form() next button.
 *
 * Capture the values from page one and store them away so they can be used
 * at final submit time.
 */
function add_student_next_submit($form, &$form_state) {
  // Values are saved for each page.
  // to carry forward to subsequent pages in the form.
  // and we tell FAPI to rebuild the form.
  $form_state['page_values'][1] = $form_state['values'];
  if (!empty($form_state['page_values'][2])) {
    $form_state['values'] = $form_state['page_values'][2];
  }
  // When form rebuilds, it will look at this to figure which page to build.
  $form_state['page_num'] = 2;
  $form_state['rebuild'] = TRUE;
}
/**
 * Back button handler submit handler.
 *
 * Since #limit_validation_errors = array() is set, values from page 2
 * will be discarded. We load the page 1 values instead.
 */
function add_student_two_back() {
  //$form_state['values'] = $form_state['page_values'][1];
  //$form_state['page_num'] = 1;
  //$form_state['rebuild'] = TRUE;
  $html = '
  <script type"text/javascript">
  window.history.go(-1);
  </script>
  ';
  return $html;
}
/**
 * The page 2 submit handler.
 *
 * This is the final submit handler. Gather all the data together and output
 * it in a drupal_set_message().
 */
function add_student_two_submit($form, &$form_state) {
  // Normally, some code would go here to alter the database with the data
  // collected from the form. Instead sets a message with drupal_set_message()
  // to validate that the code worked.
  $page_one_values = $form_state['page_values'][1];

  $password = user_password(8);
  // Role to grant the permissions to
  $wali_role = user_role_load_by_name('wali');
  $wali_rid = $wali_role->rid;

    //set up the user fields
  $fields = array(
    'name' => $page_one_values['nis'],
    'mail' => $form_state['values']['mail'],
    'pass' => $password,
    'status' => 1,
    'signature_format' => 'filtered_html',
    'init' => $form_state['values']['mail'],
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      $wali_rid => 'wali',
    ),
  );

  //the first parameter is left blank so a new user is created
  $account = user_save(NULL, $fields);

// debug db_insert susah bro
  $sql = db_insert('sia_students')
    ->fields(array(
        'suid' => $account->uid,
        'nis' => $page_one_values['nis'],
        'student_name' => $page_one_values['name'],
        'birthday' => date('Y-m-d',strtotime($page_one_values['birthdate'])),
        'birth_place' => $page_one_values['birthplace'],
        'student_address' => $page_one_values['address'],
        'student_gender' => $page_one_values['jk'],
        'religion' => $page_one_values['religion'],
        'register' => date('Y-m-d'),
        'guardian_name' => $form_state['values']['guardian_name'],
        'guardian_address' => $form_state['values']['guardian_address'],
        'job' => $form_state['values']['job'],
        'phone' => $form_state['values']['telp'],
        'student_status' => 'aktif',
        'class_name' => $page_one_values['class'],
    ));
  $runsql = $sql->execute();

  // If you want to send the welcome email, use the following code
  // Manually set the password so it appears in the e-mail.
  $account->password = $fields['pass'];

  // Send the e-mail through the user module.
  drupal_mail($page_one_values['nis'], 'register_no_approval_required', $form_state['values']['mail'], NULL, array('account' => $account), variable_get('site_mail', 'sia@drupal.com'));
}

/**
 * List of students
 */
  function view_student($name=null, $class=null, $status=null){
  $header = array(
    array('data' => t('No'), 'style' => 'text-align:center'),
    array('data' => t('Nomor Induk Siswa'), 'field' => 's.nis', 'sort'=>'asc', 'style' => 'text-align:center'),
    array('data' => t('Nama Siswa'), 'field' => 's.student_name', 'style' => 'text-align:center'),
    array('data' => t('Kelas'), 'field' => 's.class_name', 'style' => 'text-align:center'),
  );
  $sql = db_select('sia_students', 's')
      ->fields('s')
      ->extend('PagerDefault')
        ->limit(10)
      ->extend('TableSort')
      ->orderByHeader($header);
  if(!empty($_GET['name'])){ $sql->condition('s.student_name', db_like($_GET['name']) . '%', 'LIKE'); }
  if(!empty($_GET['class'])){ $sql->condition('s.class_name',$_GET['class']); }
  if(!empty($_GET['status'])){ $sql->condition('s.student_status',$_GET['status']); }
  $query = $sql->execute();

  $limit=10;
  if (isset($_GET['page'])){
    $i=$limit*$_GET['page']+1;
  }
  else{
    $i=1;
  }
  $rows = array();
  foreach ($query AS $result){
    $rows[] = array(
      array('data' => $i++, 'style' => 'text-align:center'),
      array('data' => l(check_plain($result->nis),"students/{$result->suid}/detail-student")),
      array('data' => l(check_plain($result->student_name),"students/{$result->suid}/detail-student")),
      array('data' => $result->class_name, 'style' => 'text-align:center'),
    );
  }

  $tbl = theme("table", array('header' => $header, 'rows' => $rows, 'empty' => t('Tidak ada data yang ditemukan.')));
  $tbl .= theme('pager', array('tags' => array()));
  $tbl .= l('<br /><button>' . t('Tambah') . '</button>', 'students/register-student', array('html' => TRUE));

  $build['output'] = array(
        'form_search' => drupal_get_form('student_search_form'),
        'show_table' => array('#markup' => $tbl, ),
    );

    return $build;
  }

/**
  * Function detail student
  */
  function detail_student($arg1){
  $suid = $arg1;
  $sql = db_select('sia_students', 's')
      ->fields('s')
      ->condition('suid',$suid);
  $result = $sql->execute();

    if ($result->rowCount() < 1){
      drupal_set_message('Data tidak ditemukan.');
      header("Location: view-student");
    }
    else{
      $row = array();
      foreach($result as $record){
        $row = (array)$record;
      }
    }

  if ($row['student_gender'] == 'L') $gender='Laki-laki';
  else $gender = 'Perempuan';
  $graduate = array();
  $graduate_val = array();

  if (isset($row['graduate'])) {
    $graduate = array(
      '#markup' => t("Tanggal {$row['student_status']}"),
      '#prefix' => '<tr><td width=25%>',
      '#suffix' => '</td>',
      );
      $graduate_val = array(
      '#markup' => t(" : {$row['graduate']}" ),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>'
      );
  }

  $output =  array(
    'judul' => array(
      '#markup' => "<b><h2>Biodata siswa</h2></b>",
    ),
    'start_table' => array(
    '#suffix' => "<div><table style='background-color:#FFF' >",
    ),
    'nis' => array(
    '#markup' => t("Nomor Induk Siswa (NIS)  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'nis_val' => array(
    '#markup' => t(" : {$row['nis']}" ),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'class' => array(
    '#markup' => t("Kelas  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'class_val' => array(
    '#markup' => t(" : {$row['class_name']}" ),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'status' => array(
    '#markup' => t("Status"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'status_val' => array(
    '#markup' => t(" : {$row['student_status']}" ),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    //muncul hanya saat siswa sudah lulus
    'graduate' => $graduate,
    'graduate_val' => $graduate_val,
    //===================================
    'name' => array(
    '#markup' => t("Nama Siswa  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'name_val' => array(
    '#markup' => t(" : {$row['student_name']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'birthday' => array(
    '#markup' => t("Tempat/tanggal lahir  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'birthday_val' => array(
    '#markup' => t(" : {$row['birth_place']}, {$row['birthday']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'jk' => array(
    '#markup' => t("Jenis Kelamin  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'jk_val' => array(
    '#markup' => t(" : {$row['student_gender']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'jk' => array(
    '#markup' => t("Jenis Kelamin  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'jk_val' => array(
    '#markup' => t(" : {$gender}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'agama' => array(
    '#markup' => t("Agama"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'agama_val' => array(
    '#markup' => t(" : {$row['religion']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'guardian_name' => array(
    '#markup' => t("Orang tua wali"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'guardian_name_val' => array(
    '#markup' => t(" : {$row['guardian_name']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'guardian_adr' => array(
    '#markup' => t("Alamat orang tua wali"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'guardian_adr_val' => array(
    '#markup' => t(" : {$row['guardian_address']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'job' => array(
    '#markup' => t("Pekerjaan"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'job_val' => array(
    '#markup' => t(" : {$row['job']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'phone' => array(
    '#markup' => t("Telp."),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'phone_val' => array(
    '#markup' => t(" : {$row['phone']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'registrasi' => array(
    '#markup' => t("Tanggal registrasi siswa"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'registrasi_val' => array(
    '#markup' => t(" : {$row['register']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'end_table' => array(
    '#suffix' => '</table>',
    ),
  );

   //dpm($row);
  return render($output);

  //header("Location:view-student");
  }

/**
 * function for edit student data
 */

  function edit_student($form, &$form_state, $suid){
  $sql = db_select('sia_students', 's')
      ->fields('s')
      ->condition('suid',$suid);
  $result = $sql->execute();

    if ($result->rowCount() < 1){
      drupal_set_message('Data tidak ditemukan.');
      drupal_goto('students/view-student');
    }
    else{
      $student = array();
      foreach($result as $record){
        $student = (array)$record;
      }
    }

  $class = db_query('select class_name from sia_class');

  if ( $class->rowCount() < 1 ){
    t('Silahkan isi dulu data kelas');
  }
  else{
      $row_class=array();
      foreach ($class as $row){
        $row_class[$row->class_name] = $row->class_name;
      }
  }

  $status = array(
    'aktif' => 'Aktif',
    'lulus' => 'Lulus',
    'meninggal' => 'Meninggal',
    'dikeluarkan' => 'Dikeluarkan',
    'berhenti' => 'Berhenti',
  );

  $jk = array(
    'L' => 'Laki-laki',
    'P' => 'Perempuan',
  );

  $ag = array(
    'Islam' => 'Islam',
    'Katolik' => 'Katolik',
    'Kristen' => 'Kristen',
    'Budha' => 'Budha',
    'Hindu' => 'Hindu',
  );

  //form edit
  $form = array();
  $form['suid'] = array(
    '#type' => 'hidden',
    '#default_value' => $suid,
  );
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Data siswa.'),
  );
  $form['nis'] = array(
    '#type' => 'textfield',
    '#title' => t('NIS'),
    '#size' =>12,
    '#maxlength' => 10,
    '#disabled' => TRUE,
    '#default_value' => $student['nis'],
  );
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $student['student_status'],
    '#options' => $status,
    '#ajax' => array(
      'callback' => 'date_field_callback',
      'wrapper' => 'textfields',
      'effect' => 'fade',
    ),
  );

  $form['textfields'] = array(
    '#prefix' => '<div id="textfields">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
  );
  //cek status
  if(!empty($student['graduate'])){
    $form['textfields']['graduate'] = array(
     '#title' => t('Tanggal %status',array('%status'=>$student['student_status'])),
     '#default_value' => $student['graduate'],
     '#type' => 'date_popup',
     '#date_format' => 'Y-m-d',
    );
  }
  // Since checkboxes return TRUE or FALSE, we have to check that
  // $form_state has been filled as well as what it contains.
  if (!empty($form_state['values']['status']) && $form_state['values']['status']!='aktif') {
    $st = $form_state['values']['status'];
    $form['textfields']['graduate'] = array(
      //'#type' => 'textfield',
      '#title' => t('Tanggal %status',array('%status'=>$st)),
      '#default_value' => '',
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
    );
  }

 $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama lengkap'),
    '#size' =>35,
    '#maxlength' => 35,
    '#required' => TRUE,
    '#default_value' => $student['student_name'],
  );
  $form['address'] = array(
    '#type' => 'textarea',
    '#title' => t('Alamat'),
    '#size' =>50,
    '#required' => TRUE,
    '#default_value' =>$student['student_address'],
  );
  $form['birthdate'] = array(
    '#title' => t('Tanggal lahir'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $student['birthday'],
    '#attributes' => array('class' => array('datepicker')),
  );
  $form['birthplace'] = array(
    '#type' => 'textfield',
    '#title' => t('Tempat lahir'),
    '#size' =>50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>$student['birth_place'],
  );
  $form['jk'] = array(
    '#type' => 'radios',
    '#title' => t('Jenis Kelamin'),
    '#description' => t(''),
    '#default_value' =>  variable_get('jk', $student['student_gender']),
    '#options' => $jk,
  );
  $form['religion'] = array(
    '#title' => t('Agama'),
    '#type' => 'select',
    '#multiple_toggle' => '1',
    '#default_value' =>variable_get('religion',$student['religion']),
    '#options' => $ag,
  );
  $form['class'] = array(
    '#title' => t('Kelas'),
    '#type' => 'select',
    '#multiple_toggle' => '1',
    '#default_value' =>variable_get('class',$student['class_name']),
    '#options' => $row_class,
  );
  $form['guardian_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama lengkap wali'),
    '#size' =>35,
    '#maxlength' => 35,
    '#required' => TRUE,
    '#default_value' =>$student['guardian_name'],
  );
  $form['guardian_address'] = array(
    '#type' => 'textarea',
    '#title' => t('Alamat'),
    '#size' =>50,
    '#required' => TRUE,
    '#default_value' =>$student['guardian_address'],
  );
  $form['job'] = array(
    '#type' => 'textfield',
    '#title' => t('Pekerjaan'),
    '#size' =>50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>$student['job'],
  );
  $form['telp'] = array(
    '#type' => 'textfield',
    '#title' => t('Telp.'),
    '#size' =>20,
    '#maxlength' => 12,
    '#required' => TRUE,
    '#default_value' =>$student['phone'],
    '#attributes' =>array('placeholder' => t('0850000000000'))
  );
  $form['register'] = array(
    '#title' => t('Tanggal registrasi'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $student['register'],
    '#attributes' => array('class' => array('datepicker')),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
    //'#validate' => array('add_student_two_validate'),
    //'#submit' => array('add_student_two_submit'),
  );

  //penting biar date picker jalan
  $form['#after_build'] = array('sia_sekolah_form_uidatepicker');
  //$_GET['destination'] = "students/{$suid}/detail-student";
  return $form;

  }

/**
 * Selects the piece of the form we want to use as replacement text and returns
 * it as a form (renderable array).
 *
 * @return renderable array (the textfields element)
 */
function date_field_callback($form, $form_state) {
  return $form['textfields'];
}


/**
 * edit student form submit handler
 */
 function edit_student_submit($form, &$form_state){
  $grd = null;

  if(!empty($form_state['values']['graduate']) && ($form_state['values']['status'] != 'aktif') ){
    $grd = $form_state['values']['graduate'];
    // block user yang tidak aktif
    // load user object
    $existingUser = user_load($form_state['values']['suid']);
    // update some user property
    $existingUser->status = 0;
    // save existing user
    user_save((object) array('uid' => $existingUser->uid), (array) $existingUser);
  }

  try{
    $id = db_update('sia_students')
      ->fields(array(
        'student_name' => $form_state['values']['name'],
        'birthday' => date('Y-m-d',strtotime($form_state['values']['birthdate'])),
        'birth_place' => $form_state['values']['birthplace'],
        'student_address' => $form_state['values']['address'],
        'student_gender' => $form_state['values']['jk'],
        'religion' => $form_state['values']['religion'],
        'register' => date('Y-m-d',strtotime($form_state['values']['register'])),
        'guardian_name' => $form_state['values']['guardian_name'],
        'guardian_address' => $form_state['values']['guardian_address'],
        'job' => $form_state['values']['job'],
        'phone' => $form_state['values']['telp'],
        'class_name' => $form_state['values']['class'],
        'student_status' => $form_state['values']['status'],
        'graduate' => isset($grd) ? date('Y-m-d',strtotime($grd)) : $grd,
        ))
      ->condition('suid',$form_state['values']['suid'])
      ->execute();
    drupal_set_message(t('Data telah diubah'));
  }
  catch(PDOException $e){
    drupal_set_message(t('Error : %message',array('%message'=>$e->getMessage())),'error');
  }
 }

/**
 * datepicker
 */
 function sia_sekolah_form_uidatepicker($form, $form_state) {
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js("(function ($) { $('.datepicker').datepicker(); })(jQuery);", array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
  return $form;
}

/**
 * custom search form
 */
 function student_search_form($form, &$form_state) {
  $form = array();
  $status = array(
    'aktif' => 'Aktif',
    'lulus' => 'Lulus',
    'meninggal' => 'Meninggal',
    'dikeluarkan' => 'Dikeluarkan',
    'berhenti' => 'Berhenti',
  );
  $class = db_query('select class_name from sia_class');
    foreach ($class as $row){
        $row_class[$row->class_name] = $row->class_name;
      }
  $form['advance_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advance search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance_search']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama siswa'),
    '#default_value' => "",
    '#description' => "Masukkan nama siswa yang dicari.",
    '#size' => 20,
    '#maxlength' => 20,
  );
  $form['advance_search']['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
    ),
  );
  $form['advance_search']['container']['class'] = array(
    '#type' => 'select',
    '#title' => t('Cari berdasarkan '),
    '#empty_option' => '- ' . t('kelas') . ' -',
    '#options' => $row_class,
  );
  $form['advance_search']['container']['status'] = array(
    '#type' => 'select',
    '#empty_option' => '- ' . t('status') . ' -',
    '#options' => $status,
  );
  $form['advance_search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
  );

  return $form;
 }

 /**
 * custom search form submit handler
 */
 function student_search_form_submit($form, &$form_state) {
  $path = t('students/view-student/');
  $options = array(
    'query' => array(
      'name' => $form_state['values']['name'],
      'class' => $form_state['values']['class'],
      'status' => $form_state['values']['status'],
      )
    );
  return drupal_goto($path, $options);
 }

