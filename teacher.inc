<?php
/**
 * register new teacher
 */
 function register_teacher(){
  drupal_get_form('add_teacher_form');
 }
/**
 * add new teacher
 */
 function add_teacher_form($form, &$form_state, $tuid=null, $aksi=null){

  $status = array(
    'aktif' => 'Aktif',
    'mutasi' => 'Mutasi',
    'pensiun' => 'Pensiun',
  );

  $jk = array(
    'L' => 'Laki-laki',
    'P' => 'Perempuan',
  );

  $ag = array(
    'Islam' => 'Islam',
    'Katolik' => 'Katolik',
    'Kristen' => 'Kristen',
    'Budha' => 'Budha',
    'Hindu' => 'Hindu',
  );

  $mapel = db_query('select * from sia_kkm');

  if ( $mapel->rowCount() < 1 ){
    t('Silahkan isi dulu data kkm.');
  }
  else{
      $row_kkm=array();
      foreach ($mapel as $row){
        $row_kkm[$row->kkm_id] = $row->subject_name;
      }
  }

  // Otherwise we build page 1.
  $form_state['page_num'] = 1;
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Input data Guru.'),
  );

  if($aksi == 'edit'){
  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => $status,
    '#ajax' => array(
      'callback' => 'date_field_callback',
      'wrapper' => 'textfields',
      'effect' => 'fade',
    ),
  );

  $form['textfields'] = array(
  '#prefix' => '<div id="textfields">',
  '#suffix' => '</div>',
  '#type' => 'fieldset',
  );

  // Since checkboxes return TRUE or FALSE, we have to check that
  // $form_state has been filled as well as what it contains.
  if (!empty($form_state['values']['status']) && $form_state['values']['status']!='aktif') {
    $st = $form_state['values']['status'];
    $form['textfields']['leave_date'] = array(
      //'#type' => 'textfield',
      '#title' => t("Tanggal {$st}"),
      '#default_value' => '',
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
    );
  }

  }

  $form['nip'] = array(
    '#type' => 'textfield',
    '#title' => t('NIP'),
    '#description' => "Masukkan nomor induk pegawai.",
    '#size' =>35,
    '#maxlength' => 18,
    '#default_value' =>'',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama lengkap'),
    '#size' =>35,
    '#maxlength' => 35,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  if($aksi==null){
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => "Username untuk login sia.",
    '#size' =>35,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>'',
    '#validate' => array('username_validate'),
  );
  }
  $form['address'] = array(
    '#type' => 'textarea',
    '#title' => t('Alamat'),
    '#size' =>50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['birthplace'] = array(
    '#type' => 'textfield',
    '#title' => t('Tempat lahir'),
    '#size' =>50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' =>'',
  );
  $form['birthdate'] = array(
  '#title' => t('Tanggal lahir'),
  '#type' => 'date_popup',
  '#date_format' => 'Y-m-d',
  '#required' => TRUE,
  '#date_year_range' => '-55:-18',
  );
  $form['jk'] = array(
    '#type' => 'radios',
    '#title' => t('Jenis Kelamin'),
    '#description' => t(''),
    '#default_value' =>  variable_get('jk', 'L'),
    '#options' => $jk,
  );
  $form['religion'] = array(
    '#title' => t('Agama'),
    '#type' => 'select',
    '#multiple_toggle' => '1',
    '#options' => $ag,
  );
  if($aksi==null){
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' =>50,
    '#maxlength' => 54,
    '#required' => TRUE,
    '#default_value' =>'',
    '#element_validate' => array('email_validate'),
    '#attributes' =>array('placeholder' => t('example@email.com'))
  );
  }
  $form['telp'] = array(
    '#type' => 'textfield',
    '#title' => t('Telp/HP.'),
    '#size' =>20,
    '#maxlength' => 12,
    '#required' => TRUE,
    '#default_value' =>'',
    '#attributes' =>array('placeholder' => t('0850000000000')),
    '#element_validate' => array('element_validate_number')
  );
  $form['join'] = array(
  '#title' => t('Tanggal masuk instansi'),
  '#type' => 'date_popup',
  '#date_format' => 'Y-m-d',
  '#required' => TRUE,
  '#date_year_range' => '-50:0',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );
  //penting biar date picker jalan
  $form['#after_build'] = array('sia_sekolah_form_uidatepicker');

  if (isset($tuid) && $aksi=='edit'){
      try{
        $sql = db_select('sia_teachers','t')
          ->fields('t')
          ->condition('tuid',$tuid)
          ->execute()
          ->fetchAssoc();
        $sql_user = db_select('users','u')
          ->fields('u')
          ->condition('uid',$tuid)
          ->execute()
          ->fetchAssoc();
          //dpm($sql_user);
      }
      catch (PDOException $e){
        drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
      }
      if(!empty($sql['leave_date'])){
        //$form['status']['#disabled'] = TRUE;
        $form['textfields']['leave_date'] = array(
          '#title' => t('Tanggal pensiun'),
          '#default_value' => $sql['leave_date'],
          // '#disabled' => TRUE,
          '#type' => 'date_popup',
          '#date_format' => 'Y-m-d',
          '#date_year_range' => '-10:0',
        );
      }
      $form['description']['#title'] = t('Edit data guru');
      $form['tuid']['#type'] = 'hidden';
      $form['tuid']['#default_value'] = $sql['tuid'];
      $form['nip']['#default_value'] = $sql['nip'];
      $form['name']['#default_value'] = $sql['name'];
      $form['status']['#default_value'] = variable_get('status', $sql['status']);
      $form['address']['#default_value'] = $sql['address'];
      $form['birthplace']['#default_value'] = $sql['birth_place'];
      $form['jk']['#default_value'] = variable_get('jk',$sql['gender']);
      $form['telp']['#default_value'] = $sql['phone'];
      $form['religion']['#default_value'] = $sql['religion'];
      $form['birthdate']['#default_value'] = $sql['birthday'];
      $form['join']['#default_value'] = $sql['join_date'];
      $form['submit']['#submit'] = array('edit_teacher_form_submit');
   }
  return $form;
}

/**
 * Selects the piece of the form we want to use as replacement text and returns
 * it as a form (renderable array).
 *
 * @return renderable array (the textfields element)
 */
function date_field_callback($form, $form_state) {
  return $form['textfields'];
}

/**
 * Validate handler for teacher username.
 */
function username_validate($form, &$form_state) {

  // validate username
  $username = db_select('users','u')
    ->fields('u')
    ->condition('name',$form_state['values']['username'])
    ->execute();
  if ($nis->rowCount() > 0){
    form_set_error('username',t('Username telah digunakan.'));
  }
}

/**
 * Validate handler for the submit button on add_teacher_form.
 */
function email_validate($form, &$form_state) {

  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail',t('Pastikan format email benar.'));
  }
}

/**
  * submit handler add_teacher_form
  */
 function add_teacher_form_submit($form, &$form_state) {

  $password = user_password(8);
  // Role to grant the permissions to
  $guru_role = user_role_load_by_name('guru');
  $guru_rid = $guru_role->rid;

    //set up the user fields
  $fields = array(
    'name' => $form_state['values']['username'],
    'mail' => $form_state['values']['mail'],
    'pass' => $password,
    'status' => 1,
    'signature_format' => 'filtered_html',
    'init' => $form_state['values']['mail'],
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      $guru_rid => 'guru',
    ),
  );

  //the first parameter is left blank so a new user is created
  try{
  $account = user_save(NULL, $fields);
  }
  catch (PDOException $e) {
  drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
}

// debug db_insert susah bro
try{
  $sql = db_insert('sia_teachers')
    ->fields(array(
        'tuid' => $account->uid,
        'nip' => $form_state['values']['nip'],
        'name' => $form_state['values']['name'],
        'birthday' => date('Y-m-d',strtotime($form_state['values']['birthdate'])),
        'birth_place' => $form_state['values']['birthplace'],
        'address' => $form_state['values']['address'],
        'gender' => $form_state['values']['jk'],
        'religion' => $form_state['values']['religion'],
        'join_date' => date('Y-m-d',strtotime($form_state['values']['join'])),
        'phone' => $form_state['values']['telp'],
        'status' => 'aktif',
      ));
  $runsql = $sql->execute();
}
catch (PDOException $e) {
  drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
}
  // If you want to send the welcome email, use the following code
  // Manually set the password so it appears in the e-mail.
//  $account->password = $fields['pass'];

  // Send the e-mail through the user module.
  drupal_mail($form_state['values']['username'], 'register_no_approval_required', $form_state['values']['mail'], NULL, array('account' => $account), variable_get('site_mail', 'sia@drupal.com'));
}

/**
 * submit handles function edit teacher data
 */
 function edit_teacher_form_submit($form, &$form_state){
  $ret = null;

  if(!empty($form_state['values']['leave_date']) && ($form_state['values']['status'] == 'pensiun') ){
    $ret = $form_state['values']['leave_date'];
    // block user yang pensiun
    // load user object
    $existingUser = user_load($form_state['values']['tuid']);
    // update some user property
    $existingUser->status = 0;
    // save existing user
    user_save((object) array('uid' => $existingUser->uid), (array) $existingUser);
  }
  try{
    $sql = db_update('sia_teachers')
      ->fields(array(
          'nip' => $form_state['values']['nip'],
          'name' => $form_state['values']['name'],
          'birthday' => date('Y-m-d',strtotime($form_state['values']['birthdate'])),
          'birth_place' => $form_state['values']['birthplace'],
          'address' => $form_state['values']['address'],
          'gender' => $form_state['values']['jk'],
          'religion' => $form_state['values']['religion'],
          'join_date' => date('Y-m-d',strtotime($form_state['values']['join'])),
          'phone' => $form_state['values']['telp'],
          'status' => $form_state['values']['status'],
          'leave_date' => isset($ret) ? date('Y-m-d',strtotime($ret)) : $ret,
        ))
      ->condition('tuid',$form_state['values']['tuid']);
    $runsql = $sql->execute();

    drupal_set_message(t('Data guru telah diubah'));
  }
  catch (PDOException $e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
 }


/**
 * List of teacher
 */
  function view_teacher($name=null, $status=null){

  $header = array(
  array('data' => t('No'), 'style' => 'text-align:center'),
  array('data' => t('Nomor Induk Pegawai'), 'field' => 's.nip', 'sort'=>'asc', 'style' => 'text-align:center'),
  array('data' => t('Nama Guru'), 'field' => 's.name', 'style' => 'text-align:center'),
  array('data' => t('Status'), 'field' => 's.status', 'style' => 'text-align:center'),
  );

  $sql = db_select('sia_teachers', 's')
      ->fields('s')
      ->extend('PagerDefault')
        ->limit(10)
      ->extend('TableSort')
      ->orderByHeader($header);
  if(!empty($_GET['name'])){ $sql->condition('s.name', db_like($_GET['name']) . '%', 'LIKE'); }
  if(!empty($_GET['status'])){ $sql->condition('s.status',$_GET['status']); }
  $query = $sql->execute();

    $limit=10;
    if (isset($_GET['page'])){
      $i=$limit*$_GET['page']+1;
    }
    else{
      $i=1;
    }

    $rows = array();
    foreach ($query AS $result){
      $rows[] = array(
        array('data' => $i++, 'style' => 'text-align:center'),
        array('data' => $result->nip),
        array('data' => l($result->name,"teachers/{$result->tuid}/detail-teacher")),
        array('data' => l($result->status,"teachers/{$result->tuid}/detail-teacher"), 'style' => 'text-align:center'),
      );
    }

    $tbl = theme("table", array('header' => $header, 'rows' => $rows, 'empty' => 'Tidak ada data yang ditemukan.'));
    $tbl .= theme('pager', array('tags' => array()));
    $tbl .= l('<br /><button>' . t('Tambah') . '</button>', 'teachers/register-teacher', array('html' => TRUE));

  $build['output'] = array(
        'form_search' => drupal_get_form('teacher_search_form'),
        'show_table' => array('#markup' => $tbl, ),
    );

    return $build;
  }


/**
  * Function detail teacher
  */
  function detail_teacher($tuid){
  try{
  $sql = db_select('sia_teachers', 's')
      ->fields('s')
      ->condition('tuid',$tuid);
  $result = $sql->execute();
  }
  catch(PDOException $e){
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
    if ($result->rowCount() < 1){
      drupal_set_message('Data tidak ditemukan.');
      header("Location: view-teacher");
    }
    else{
      $row = array();
      foreach($result as $record){
        $row = (array)$record;
      }
    }

  if ($row['gender'] == 'L') $gender='Laki-laki';
  else $gender = 'Perempuan';
  $leave_date = array();
  $leave_date_val = array();
if (isset($row['leave_date']))
  {
        $leave_date = array(
        '#markup' => t("Tanggal berhenti mengajar"),
        '#prefix' => '<tr><td width=25%>',
        '#suffix' => '</td>',
        );
        $leave_date_val = array(
        '#markup' => t(" : {$row['leave_date']}" ),
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>'
        );
  }

  $output =  array(
    'judul' => array(
      '#markup' => "<b><h2>Biodata Guru</h2></b>",
    ),
    'start_table' => array(
    '#suffix' => "<div><table style='background-color:#FFF' >",
    ),
    'nip' => array(
    '#markup' => t("Nomor Induk Pegawai (NIP)  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'nis_val' => array(
    '#markup' => t(" : {$row['nip']}" ),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'status' => array(
    '#markup' => t("Status"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'status_val' => array(
    '#markup' => t(" : {$row['status']}" ),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    //muncul hanya saat guru pensiun/berhenti mengajar
    'graduate' => $leave_date,
    'graduate_val' => $leave_date_val,
    //===================================
    'name' => array(
    '#markup' => t("Nama Guru"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'name_val' => array(
    '#markup' => t(" : {$row['name']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'birthday' => array(
    '#markup' => t("Tempat/tanggal lahir  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'birthday_val' => array(
    '#markup' => t(" : {$row['birth_place']}, {$row['birthday']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'jk' => array(
    '#markup' => t("Jenis Kelamin  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'jk_val' => array(
    '#markup' => t(" : {$row['gender']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'jk' => array(
    '#markup' => t("Jenis Kelamin  "),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'jk_val' => array(
    '#markup' => t(" : {$gender}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'agama' => array(
    '#markup' => t("Agama"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'agama_val' => array(
    '#markup' => t(" : {$row['religion']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'registrasi' => array(
    '#markup' => t("Tanggal mulai mengajar"),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'registrasi_val' => array(
    '#markup' => t(" : {$row['join_date']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'phone' => array(
    '#markup' => t("Telp."),
    '#prefix' => '<tr><td width=25%>',
    '#suffix' => '</td>',
    ),
    'phone_val' => array(
    '#markup' => t(" : {$row['phone']}"),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr>',
    ),
    'end_table' => array(
    '#suffix' => '</table>',
    ),
  );

   //dpm($row);
  return render($output);

  }

/**
 * edit teacher
 */
 function edit_teacher($tuid,$aksi){
  return drupal_get_form('add_teacher_form',$tuid, $aksi);
 }

/**
 * datepicker
 */
 function sia_sekolah_form_uidatepicker($form, $form_state) {
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js("(function ($) { $('.datepicker').datepicker(); })(jQuery);", array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
  return $form;
}

/**
 * custom search form
 */
 function teacher_search_form($form, &$form_state) {
  $form = array();
  $status = array(
    'aktif' => 'Aktif',
    'mutasi' => 'Mutasi',
    'pensiun' => 'Pensiun',
  );

  $form['advance_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advance search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance_search']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Nama pengajar'),
    '#default_value' => "",
    '#description' => "Masukkan nama pengajar yang dicari.",
    '#size' => 20,
    '#maxlength' => 20,
  );
  $form['advance_search']['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
    ),
  );
  $form['advance_search']['container']['status'] = array(
    '#type' => 'select',
    '#title' => t('Cari berdasarkan '),
    '#empty_option' => '- ' . t('status') . ' -',
    '#options' => $status,
  );
  $form['advance_search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
  );

  return $form;
 }

 /**
 * custom search form submit handler
 */
 function teacher_search_form_submit($form, &$form_state) {
  $path = t('teachers/view-teacher/');
  $options = array(
    'query' => array(
      'name' => $form_state['values']['name'],
      'status' => $form_state['values']['status'],
      )
    );
  return drupal_goto($path, $options);
 }
