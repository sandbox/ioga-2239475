<?php
/**
 * show raport
 */
 function show_raport($suid){
   $sql = db_select('sia_students', 's')
      ->fields('s')
      ->condition('suid',$suid);
   $result = $sql->execute();

   if ($result->rowCount() < 1){
     drupal_set_message('Data tidak ditemukan.');
     drupal_goto("");
   }
   else{

   if(!empty($_GET['th'])){
      $year=$_GET['th'];
   }
   else{
      $year_act = db_select('sia_year','y')
        ->fields('y', array('year_active'))
        ->execute()
        ->fetchAssoc();
   $year = $year_act['year_active'];
   }
   //sql single student
   $sql = db_select('sia_student_data','st');
   $sql->leftJoin('sia_students','s','s.suid = st.suid');
   $sql->leftJoin('sia_kkm','kkm','kkm.kkm_id = st.kkm_id');
   $sql->leftJoin('sia_test_result','tr','tr.test_result_id = st.test_result_id');
   //$sql->condition('kkm.class_name','s.class_name');
   $sql->condition('st.year_active',$year);
   $sql->condition('st.suid',$suid);
   $run_sql = $sql
        ->fields('s', array('student_name','nis'))
        ->fields('kkm',array('subject_name','class_name','kkm'))
        ->fields('st')
        ->fields('tr', array(
            'last_result',
            ))
        ->execute();
   //dpm($run_sql);
   $i=1;

    $header = array(
      array('data' => t('No'), 'style' => 'text-align:center'),
      array('data' => t('Mata Pelajaran'), 'style' => 'text-align:center'),
      array('data' => t('Nilai'), 'style' => 'text-align:center'),
      array('data' => t('KKM'), 'style' => 'text-align:center'),
      array('data' => t('Ketuntasan'), 'style' => 'text-align:center'),
      );
   $rows = array();
   $data = array();
   $rata = 0;
   $total = 0;
   foreach ($run_sql AS $result){
     $data = (array) $result;
     $rows[] = array(
      array('data' => $i++, 'style' => 'text-align:center'),
      array('data' => $result->subject_name),
      array('data' => $result->last_result, 'style' => 'text-align:center'),
      array('data' => $result->kkm, 'style' => 'text-align:center'),
      array('data' => $result->last_result > $result->kkm ? t("Tuntas") : t("Tidak tuntas"), 'style' => 'text-align:center'),
    );
   $total = $total + $result->last_result;
   }
   if(sizeof($rows)){
   $rata = round($total/sizeof($rows),2);
   $ket = t("<hr />
            <div class='cc'>
            <div class='satu'>
               Tahun Ajaran : {$year}<br />
               Nomor Induk : {$data['nis']}
            </div>
            <div class='dua'>
               Nama : {$data['student_name']}<br />
               Kelas : {$data['class_name']}
            </div>
            </div><hr/>
            ");
   }else $ket=t("<hr />Belum ada data tahun ajaran {$year}<hr/>
            ");

   //css inline
   drupal_add_css(".cc{ overflow:auto; }", 'inline');
   drupal_add_css(".satu{ width:300px; float:left; }", 'inline');
   drupal_add_css(".dua{ width:300px; float:right; }", 'inline');

   $tbl = theme("table", array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('Tidak ada data ditemukan.')
      ));
   $tbl .= theme('pager', array('tags' => array()));
   //cek role user
   global $user;
   $check = array_intersect(array('guru', 'administrator'), array_values($user->roles));
   $build['output'] = array(
        'form_search' => drupal_get_form('raport_search_form',$suid),
           'keterangan' => array(
               '#markup' => $ket,
               ),
        'show_table' => array('#markup' => $tbl),
        'rata' => array(
               '#markup' => t("
                           <hr />
                           Total nilai : {$total}<br />
                           Rata - rata : {$rata} <br />
                        ") ),
         'detail' => empty($check) ? drupal_get_form('detail_raport_form',$suid) : drupal_get_form('edit_detail_raport_form',$suid),
   );

   return $build;
   }
 }

/**
 * custom search form
 */
 function raport_search_form($form, &$form_state, $suid) {
  $form = array();
  $year = db_query("SELECT distinct(year_active) from sia_kkm left join sia_student_data using(kkm_id) where (sia_student_data.suid={$suid})");
    foreach ($year as $row){
        $available_year[$row->year_active] = $row->year_active;
      }
  $form['suid'] = array(
    '#type' => 'hidden',
    '#default_value' => $suid,
  );
  $form['advance_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance_search']['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
    ),
  );
  $form['advance_search']['container']['year'] = array(
    '#type' => 'select',
    '#title' => t('Cari berdasarkan  '),
    '#empty_option' => '- ' . t('tahun ajaran') . ' -',
    '#options' => $available_year,
    '#ajax' => array(
      'callback' => 'class_field_callback',
      'wrapper' => 'textfields',
      'effect' => 'fade',
    ),
  );
  $form['advance_search']['container']['class'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="textfields">',
    '#suffix' => '</div>',
  );
  if (!empty($form_state['values']['year'])) {
    $yr = $form_state['values']['year'];
    $sql_class = db_query("select distinct(class_name) from sia_kkm left join sia_student_data using(kkm_id) where (sia_student_data.year_active='{$yr}')");
    foreach ($sql_class as $class);
    $form['advance_search']['container']['class']['#title'] = t("Kelas {$class->class_name}");
  }
  $form['advance_search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['br'] = array(
    '#type' => 'item',
    '#preffix' => t('<br />'),
  );

  return $form;
 }

/**
 * custom search form submit handler
 */
 function raport_search_form_submit($form, &$form_state) {
   $suid = $form_state['values']['suid'];
  $path = t("students/{$suid}/detail-student/raport/");
  $options = array(
    'query' => array(
      'th' => $form_state['values']['year'],
      )
    );
  return drupal_goto($path, $options);
 }

// callback ajax class
function class_field_callback($form, $form_state) {
  return $form['advance_search']['container']['class'];
}

// function tambahan utk admin
 function edit_detail_raport_form($form, &$form_state, $suid){
   $form['suid'] = array(
    '#type' => 'hidden',
    '#default_value' => $suid,
  );
   $form['note'] = array(
    '#type' => 'textarea',
    '#title' => t('Catatan pengembangan diri'),
    '#default_value' => t(''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
  );
  return $form;
 }
// function tambahan untuk siswa
 function detail_raport_form($form, &$form_state, $suid){
   $form['note'] = array(
    '#type' => 'textarea',
    '#title' => t('Catatan pengembangan diri'),
    '#default_value' => t(''),
  );
  return $form;
 }
