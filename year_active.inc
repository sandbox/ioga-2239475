<?php
/**
 * function to show current active year
 */
 function view_year(){
    $sql = db_select('sia_year','s')
        ->fields('s');
    $run_sql = $sql->execute();
    //dpm($run_sql);
    if($run_sql->rowCount() < 1){
        $th = date('Y');
        $th2 = date('Y')+1;
        $set = db_insert('sia_year')
            ->fields(array(
                'year_active' => $th."/".$th2 ." - smt 1",
                'edited' => date('Y-m-d H:m:s'),
                ));
        $run_set = $set->execute();
        $run_sql = $sql->execute();
    }

    $row = array();
      foreach($run_sql as $record){
        $row = (array)$record;
      }
    //dpm($row);
    $output = t("Tahun ajaran saat ini adalah {$row['year_active']}");
    return render($output);
 }

 /**
  * function edit tahun ajaran
  */
  function edit_year($form, &$form_state){
    $sql = db_select('sia_year','y')
        ->fields('y')
        ->execute();

    $row = array();
    foreach($sql as $record){
       $row = (array)$record;
    }

    $form = array();
    $form['year_old'] = array(
    '#type' => 'hidden',
    '#default_value' => $row['year_active'],
    );
    $form['year'] = array(
    '#title' => t('Tahun ajaran yang berjalan'),
    '#type' => 'textfield',
    '#description' => t('<i>Contoh format : 2014/2015 - smt 1 atau 2014/2015 - smt 2</i>'),
    '#size' => 25,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#default_value' => $row['year_active'],
    );
    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Simpan'),
    );

    $_GET['destination'] = 'years/active-semester';
    return $form;
  }

  /**
   * submit handler for edit_year form
   */
  function edit_year_submit($form, &$form_state){

    $sql = db_update('sia_year')
        ->fields(array(
            'year_active' => $form_state['values']['year'],
            'edited' => date('Y-m-d H:m:s'),
            ))
        ->condition('year_active',$form_state['values']['year_old'])
        ->execute();

    drupal_set_message(t('Perubahan telah disimpan.'));
  }

